$(document).ready(function(){
	
//JQuery UI slider, 25m2 min and 2500m2 max

	$("#slider").slider({
		range: "min",
		min: 25,
		max: 2750,
		value: 550,

//Reflecting slider changes on input field and showing tooltip with current m2 selection

		slide: function (event, ui){
			$("#altvalue").val(ui.value);
            $('.ui-slider-handle').html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + ui.value + 'm<sup>2</sup></div></div>');

            //Tooltip position adjustment depending on m2 number
            if(ui.value > 999){
				$(".tooltip-inner").css("margin-left","-0.5em");
			}
			if(ui.value < 100){
				$(".tooltip-inner").css("margin-left","0.6em");
			}
    }
});

//On releasing slider handle, tooltip disappears

$(".ui-slider-handle").mouseleave(function() {
$(".ui-slider-handle").html("");
}); 

//Slider tooltip on hovering or dragging sliderhandle (using Jquery UI)

$(".ui-slider-handle").mouseenter(function() {
var value = $( "#slider").slider("option", "value");
$(".ui-slider-handle").html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + value + 'm<sup>2</sup></div></div>');

//Tooltip position adjustment depending on m2 number
	if(value > 999){
		$(".tooltip-inner").css("margin-left","-0.5em");
			}
	if(value < 100){
				$(".tooltip-inner").css("margin-left","0.6em");
			}
});

// Reflect input changes on slider

$("#altvalue").keyup(function() {
    $("#slider").slider("value", $(this).val());
});

//Changle slider-range background

$(".ui-slider-range").css("background","#929f00");

/* Possible use -> Input validation (25 - 2500 m2) and saving value in local storage for the next steps 

$(".forwardbtn").click(function (){
	if($("#altvalue").val() >= 25 && $("#altvalue").val() <= 2500)
		localStorage.setItem("selectedarea", $("#altvalue").val());
	else alert("Bitte Ihre Flächenangabe überprüfen");
});
*/

});

